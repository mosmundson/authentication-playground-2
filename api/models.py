from pydantic import BaseModel
from jwtdown_fastapi.authentication import Token


# Common fields for all accounts
class Account(BaseModel):
    username: str
    email: str


# What we receive from the login form
class AccountIn(Account):
    password: str


# What we return to the browser
class AccountOut(Account):
    id: int


# What we get from the database
class AccountOutWithHashedPassword(Account):
    id: int
    hashed_password: str


# The JWT token plus the account data
class AccountToken(Token):
    account: AccountOut


class HttpError(BaseModel):
    detail: str


class AuthenticationException(Exception):
    pass
